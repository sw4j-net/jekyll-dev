# Introduction

This project creates a docker image with jekyll installed. Additionally the port 4000 is exposed (which is needed for
`jekyll serve`.

The image can be used to develop sites interactively using jekyll.

The original repository is at https://git.sw4j.net/sw4j-net/jekyll-dev

This repository is mirrored to https://gitlab.com/sw4j-net/jekyll-dev

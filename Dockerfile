ARG CI_REGISTRY
FROM $CI_REGISTRY/sw4j-net/jekyll:latest

EXPOSE 4000

RUN <<EOF
apt-get update
apt-get -y upgrade
jekyll new install
cd install
bundle install
cd ..
rm -rf install
EOF
